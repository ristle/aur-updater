# AUR Updater

<!-- TOC -->
* [AUR Updater](#aur-updater)
  * [About](#about)
  * [Usage](#usage)
<!-- TOC -->

## About

This package has been created to manage aur packages up to date by checking updates of repo and checking outdated 
links on old packages.

**AUR updater** is written for Arch Linux and can't be used for different Linux systems for now. *Work in Progress*

## Usage

You need to create `settings.json`. **Example you can find in** [*test.json*](./test.json)

Or simply set env variable `AUR_PATH`...

```bash
docker build -t . aur_updater
docker run -it  --restart always --net host -v $PWD/settings.json:/app/settings.json aur_updater 
```

Or you can run simply in rust

```bash
cargo run --release
```