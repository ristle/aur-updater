FROM archlinux:latest

RUN pacman -Syyu base-devel git sudo gcc bash cargo  --noconfirm

RUN useradd -ms /bin/bash -G wheel ristle

RUN sed -i 's/# %wheel ALL=(ALL:ALL) NOPASSWD: ALL/%wheel ALL=(ALL:ALL) NOPASSWD: ALL/g' /etc/sudoers

RUN sudo -u ristle curl --proto '=https' --tlsv1.3 -sSf https://sh.rustup.rs -o /tmp/rust.sh && sudo -u ristle sh /tmp/rust.sh -y && \
    sudo -u ristle /home/ristle/.cargo/bin/cargo install cargo-arch

WORKDIR /app
COPY . /app

RUN chown ristle /app -R

RUN sudo -u ristle /home/ristle/.cargo/bin/cargo arch && pacman -U *zst --noconfirm

USER ristle

CMD ["sudo", "-u", "ristle", "aur-updater"]
