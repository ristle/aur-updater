use crate::notify::GotifyNotifications;
use serde::{Deserialize, Serialize};

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct SimpleConfig {
    pub path: String,
    pub server_location: Option<String>,
    pub gotify: Option<GotifyNotifications>,
    pub mock_tests: Option<bool>,
    pub timer_checker: i64, //< timer (in seconds) value for check aur pkgs. it temptorary
}

impl Default for SimpleConfig {
    fn default() -> Self {
        Self {
            path: "/tmp/aur/".to_string(),
            server_location: Some("https://ristle/api/v2/".to_string()),
            gotify: None,
            mock_tests: None,
            timer_checker: 3600,
        }
    }
}
