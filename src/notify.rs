use log::error;
use serde::{Deserialize, Serialize};

#[derive(Debug, Serialize, Deserialize, Clone, Hash, Eq, PartialEq)]
pub struct GotifyNotifications {
    pub gotify_server: String,
    pub gotify_token: String,
    pub message: Option<String>,
    pub message_title: Option<String>
}

fn push_notify(gotify: &GotifyNotifications) -> Result<(), String> {
    let mut headers_map = reqwest::header::HeaderMap::new();

    headers_map.append("title", reqwest::header::HeaderValue::try_from(gotify.message_title.clone().unwrap()).unwrap());
    headers_map.append("message", reqwest::header::HeaderValue::try_from(gotify.message.clone().unwrap()).unwrap());
    headers_map.append("priority", reqwest::header::HeaderValue::try_from(5).unwrap());

    let client = reqwest::blocking::Client::new();

    match client.post(format!("{}/message?token={}", gotify.gotify_server, gotify.gotify_token))
        .headers(headers_map)
        .send() {
        Ok(_) => Ok(()),
        Err(error) => {
            error!("Error sending notify {}", error.to_string());
            Err(error.to_string())
        }
    }
}

pub fn push_notify_on_error(mut gotify: Option<GotifyNotifications>, error: &str) -> Result<(), String>{
    if let Some(ref mut gotify_data) = gotify {
        gotify_data.message_title = Some("AUR Updater".to_string());
        gotify_data.message = Some(error.to_string());

        push_notify(&gotify_data)?;
    }

    Err("Gotify token is not specified!".to_string())
}