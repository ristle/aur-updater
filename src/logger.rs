use std::cmp::{max, min};
use std::collections::VecDeque;
use std::io::{BufRead, BufReader, Error};
use std::io::Write;

use ansi_escapes;
use crossterm::style::{StyledContent, Stylize};
use itertools::Itertools;
use log::debug;
use crate::errors::CmdExecuteError;

pub struct LogUpdate<W: Write> {
    stream: W,
    previous_line_count: u16,
    cursor_is_hidden: bool,
    buffer: VecDeque<StyledContent<String>>,
}

impl<W: Write> LogUpdate<W> {
    /// Create a new LogUpdate instance.
    pub fn new(mut stream: W) -> Result<Self, Error> {
        write!(stream, "{}", ansi_escapes::CursorHide)?;
        stream.flush()?;

        Ok(LogUpdate { stream, previous_line_count: 0, cursor_is_hidden: true, buffer: VecDeque::with_capacity(11) })
    }

    pub fn render(&mut self, text: &str) -> Result<(), Error> {
        self.buffer.push_back(text.to_string().dark_grey());

        let text = self.buffer.clone().into_iter().rev().take(10).join("\n");


        writeln!(self.stream, "{} {}", ansi_escapes::EraseLines(self.previous_line_count), text)?;
        self.stream.flush()?;

        self.previous_line_count = min(self.buffer.len() as u16, max(text.chars().filter(|x| *x == '\n').count() as u16, 10)) + 1;

        // debug!("Buffer size {}", self.buffer.clone().len());
        // debug!("Buffer {}", self.buffer.clone().into_iter().rev().take(10).join("\n"));
        // debug!("Prev Line Count {}", self.previous_line_count);
        // debug!("Line Count n operator: {}", text.chars().filter(|x| *x == '\n').count());
        // debug!("Line count by rust: {}",  text.lines().count());

        Ok(())
    }

    pub fn clear(&mut self) -> Result<(), Error> {
        write!(self.stream, "{}", ansi_escapes::EraseLines(self.previous_line_count))?;
        self.stream.flush()?;

        self.previous_line_count = 0;

        Ok(())
    }

    pub fn done(&mut self) -> Result<(), Error> {
        if self.cursor_is_hidden {
            write!(self.stream, "{}", ansi_escapes::CursorShow)?;
            self.stream.flush()?;
        }

        self.previous_line_count = 0;
        self.cursor_is_hidden = false;

        Ok(())
    }

    pub fn process_cmd(&mut self, mut child: std::process::Child) -> Result<(), Box<dyn std::error::Error>> {
        {
            let stdout = child.stdout.as_mut().unwrap();
            let stdout_reader = BufReader::new(stdout);
            let stdout_lines = stdout_reader.lines();

            for line in stdout_lines {
                self.render(&line.unwrap_or("".to_string()))?;
            }
        }

        self.clear()?;
        self.done()?;

        if child.wait().unwrap().success() {
            Ok(())
        } else {
            let stderr = child.stderr.as_mut().unwrap();
            let stderr_reader = BufReader::new(stderr);

            Err(Box::new(
                CmdExecuteError {
                    msg: String::from_utf8(
                        Vec::from(stderr_reader.buffer())
                    ).unwrap_or_default()
                }))
        }
    }
}

impl<W: Write> Drop for LogUpdate<W> {
    fn drop(&mut self) {
        if self.cursor_is_hidden {
            write!(self.stream, "{}", ansi_escapes::CursorShow).unwrap();
            self.stream.flush().unwrap();
        }
    }
}

/*
        let mut cmd_result = Command::new("pacman")
            .arg("-Sy")
            .arg(deps)
            .stdout(std::process::Stdio::piped())
            .stderr(std::process::Stdio::piped())
            .spawn()?;
        {
            let stdout = cmd_result.stdout.as_mut().unwrap();
            let stdout_reader = BufReader::new(stdout);
            let stdout_lines = stdout_reader.lines();

            for line in stdout_lines {
                info!("{}", line.unwrap_or("".to_string()));
            }
        }

        cmd_result.wait().unwrap();


        if cmd_result.wait().unwrap().success() {
            info!("Installed!\n\n");
            Ok(())
        } else {
            let stderr = cmd_result.stderr.as_mut().unwrap();
            let stderr_reader = BufReader::new(stderr);

            Err(Box::new(
                PKGDepsInstallError {
                    msg: String::from_utf8(
                        Vec::from(stderr_reader.buffer())
                    ).unwrap_or_default()
                }))
        }
 */
