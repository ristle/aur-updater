extern crate env_logger;

use std::error::Error;
use log::info;

extern crate chrono;
extern crate core;
extern crate timer;

mod config;
mod logger;
mod notify;
mod pkg;
mod pkgbuild_parser;
mod utils;
mod errors;

use crate::config::SimpleConfig;
use crate::utils::run;

fn main() -> Result<(), Box<dyn Error>> {
    pretty_env_logger::init();

    let timer = timer::Timer::new();

    info!("Starting aur auto updater...");

    info!("Loading config from 'settings.json'...");

    let cfg: SimpleConfig = utils::init_config();

    info!("Work directory: {}", cfg.path);

    run(&cfg)?;

    let _guard =
        timer.schedule_with_delay(chrono::Duration::seconds(cfg.timer_checker), move || {
            run(&cfg).expect("Failed to execute `aur_updater`");
        });

    loop {}
}
