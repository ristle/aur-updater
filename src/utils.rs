use std::io::Read;
use std::fs::File;
use serde::Serialize;
use std::error::Error;
use std::process::Command;
use log::{info, error};

use crate::pkg::pkg::PKG;
use crate::errors::UrlNotFound;
use crate::config::SimpleConfig;
use crate::notify::{GotifyNotifications, push_notify_on_error};



#[derive(Serialize)]
struct ArtifactsData {
    file_type: &'static str,
    pkg_name: String,
}

pub enum PKGDirectory {
    NotFound,
    Empty(String),
    Existing(String)
}

pub fn check_aur_directory(aur: String) -> PKGDirectory {
    let aur_pkgbuild = aur.clone() + "/PKGBUILD";

    let pkgbuild_path = std::path::Path::new(&aur_pkgbuild);
    let pkg_folder_path = std::path::Path::new(&aur);

    if pkgbuild_path.exists()  {
        PKGDirectory::Existing(aur.clone())
    } else if pkg_folder_path.exists() {
        PKGDirectory::Empty(aur.clone())
    } else {
        PKGDirectory::NotFound
    }
}

pub fn find_aur_pkgs(path: String) -> Vec<String> {
    let paths = std::fs::read_dir(&std::path::Path::new(&path)).unwrap().filter_map(|entry| {
        entry.ok().and_then(|e|
        e.path().file_name().and_then(|n| n.to_str().map(|s| String::from(s)))
        )
    }).collect::<Vec<String>>();

    paths
        .into_iter()
        .filter_map(|_path: String| match check_aur_directory(path.clone() + "/" + _path.as_str()){
            PKGDirectory::NotFound => {None}
            PKGDirectory::Empty(dir) => {Some(dir)}
            PKGDirectory::Existing(dir) => {Some(dir)}
        })
        .collect()
}

pub fn upload_pkg_file(pkg_file_path: &str, pkg_name: String, server_url: &str) -> Result<(), Box<dyn Error>> {
    if server_url.is_empty() {
        error!("Server URL is not provided! Exiting publishing...");
        return Err(Box::new(UrlNotFound));
    }

    let mut file = File::open(pkg_file_path)?;
    let mut contents = Vec::new();

    file.read_to_end(&mut contents)?;

    let artifacts_data = ArtifactsData {
        file_type: "ArchLinux",
        pkg_name,
    };

    let client = reqwest::blocking::Client::new();

    let form = reqwest::blocking::multipart::Form::new()
        .text("artifacts_data", serde_json::to_string(&artifacts_data)?)
        .file("file", pkg_file_path)?;

    let response = client
        .post(server_url)
        .header("artifacts_data", serde_json::to_string(&artifacts_data)?)
        .multipart(form)
        .send()?;

    info!("Package file uploaded successfully! Status: {}\n", response.status());
    Ok(())
}

pub(crate) fn init_config() -> SimpleConfig {
    let mut file = match std::fs::File::open("settings.json") {
        Ok(f) => {
            info!("Loaded config");
            f
        }
        Err(_) => {
            panic!("Failed to open file!")
        }
    };

    let mut data = String::new();
    file.read_to_string(&mut data).unwrap();

    serde_json::from_str(&data).unwrap()
}

fn update(gotify_notifications: Option<GotifyNotifications>) -> Result<(), String> {
    info!("Running system update...");

    let cmd_result = Command::new("sudo")
        .arg("pacman")
        .arg("-Syu")
        .arg("--noconfirm")
        .stdout(std::process::Stdio::inherit())
        .stderr(std::process::Stdio::piped())
        .output()
        .expect("failed to download");


    match cmd_result.status.success() {
        true => { Ok(()) }
        false => {
            let error_msg = String::from_utf8(cmd_result.stderr).unwrap_or_default();
            error!("Failed to Update system");

            push_notify_on_error(gotify_notifications, &error_msg).unwrap_or_default();
            Err(error_msg)
        }
    }
}

pub fn run(cfg: &SimpleConfig) -> Result<(), Box<dyn Error>> {
    let gotify = cfg.gotify.clone();

    match cfg.mock_tests {
        Some(_) => {
            info!("Running in mock test mode");
        }
        None => {
            update(gotify)?;
        }
    }

    let aur_pkgs = find_aur_pkgs(cfg.path.clone());

    for aur_pkg in aur_pkgs {
        info!("Processing simple package: {}",
            aur_pkg.clone().split("/").last().unwrap_or_default());

        let aur = PKG::new(aur_pkg, cfg.gotify.clone(), cfg.server_location.clone());

        aur.process()?;
    }

    Ok(())
}
