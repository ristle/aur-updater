use std::error::Error;
use std::fmt;
use std::fmt::Debug;

#[derive(Debug)]
pub struct UrlNotFound;

#[derive(Debug)]
pub struct PKGCompileError {
    pub msg: String,
}

#[derive(Debug)]
pub struct PKGDownloadError;

#[derive(Debug)]
pub struct PkgNotFound {
    pub name: String,
    pub path: String,
}

#[derive(Debug)]
pub struct PKGDepsInstallError {
    pub msg: String,
}

#[derive(Debug)]
pub struct CmdExecuteError {
    pub msg: String,
}
// ____________________

impl fmt::Display for UrlNotFound {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "Url not found... Please check before usage")
    }
}
impl Error for UrlNotFound {}

impl fmt::Display for PKGCompileError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "Failed to compile pkg... {}", self.msg)
    }
}
impl Error for PKGCompileError {}

impl fmt::Display for PKGDownloadError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "Failed to download pkg... ")
    }
}
impl Error for PKGDownloadError {}

impl fmt::Display for PkgNotFound {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "Pkg {} not found in directory {}... ", self.name, self.path)
    }
}
impl Error for PkgNotFound {}

impl fmt::Display for PKGDepsInstallError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "Failed to install pkgs: {}", self.msg)
    }
}
impl Error for PKGDepsInstallError {}

impl fmt::Display for CmdExecuteError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "Failed to execute cmd: {}", self.msg)
    }
}
impl Error for CmdExecuteError {}
