use crate::pkgbuild_parser::delimer::Delimer;

#[derive(Default, Debug, Clone)]
pub enum BlockType {
  #[default]
    None,
  Inline,
  Function,
  Array
}

#[derive(Default, Debug, Clone)]
pub struct Block {
  pub name: String,
  pub line: String,
  pub delimer: Delimer,
  pub block_type: BlockType
}

impl Block {
  fn get_delimer(line: String) -> Delimer {
    let parsed_line = line.clone().split("=").map(|x| -> String {x.to_string()}).collect::<Vec<String>>();
    if parsed_line.len() > 1 && parsed_line[1].contains('(')  && parsed_line[1].contains(')') {
      return Delimer::InlineDelimer('(');
    }

    if parsed_line.len() > 1 && parsed_line[1].contains('(') {
      return Delimer::StartDelimer('(');
    }

    if parsed_line[0].contains('{') {
      return Delimer::StartDelimer('{');
    }

    if parsed_line.len() == 2 &&
       !parsed_line[1].contains("(") &&
       !parsed_line[1].contains(")") &&
       !parsed_line[1].contains("{") {
      return Delimer::InlineDelimer('=');
    }

    if line == "}" || ( line.contains(")") && line.chars().nth(0).unwrap_or_default() != '#'  ){
      return match line.chars().nth(0) {
        Some(c) => Delimer::EndDelimer(c),
        None => Delimer::None
      }
    }

    if !line.contains("(") &&
       !line.contains(")") &&
       !line.contains("{") &&
       line.contains('=') {
      return Delimer::InlineDelimer('=');
    }

    Delimer::None
  }


  fn get_block_name(line: String) -> String {
    if line.clone().contains("()") {
      return line.clone().split("()").map(|x| -> String { x.to_string()}).collect::<Vec<String>>()[0].clone();
    } else {
      return line.clone().split("=").map(|x| -> String { x.to_string()}).collect::<Vec<String>>()[0].clone();
    }
  }

  fn get_block_type(delimer: Delimer) -> BlockType {
    let closure_inferred  = |ch: char| -> BlockType {
      match ch {
        '(' => BlockType::Array,
        '{' => BlockType::Function,
        '=' => BlockType::Inline,
        _ => BlockType::None
      }
    };
    match delimer {
      Delimer::StartDelimer(c) => {
        closure_inferred(c)
      },
      Delimer::EndDelimer(e) =>  {
        closure_inferred(e)
      },
      Delimer::InlineDelimer(i) => {
        closure_inferred(i)
      },
      Delimer::None => {
        BlockType::None
      }
    }
  }

  pub fn new(line: String) -> Self {
    let delimer = Block::get_delimer(line.clone());
    let block_type = Block::get_block_type(delimer.clone());
    Self {
      name: Block::get_block_name(line.clone()),
      line: Block::parse_line(line.clone(), delimer.clone(), block_type.clone()),
      delimer,
      block_type: block_type
    }
  }

  pub fn parse_line(line: String, delimer: Delimer, block_type: BlockType) -> String {
    let sp_line = line.clone();
    let delimer = delimer.clone();
    let splitter = | | -> String {
      let get_first_split = |delimer: char| -> String {
        sp_line.clone().split(delimer).map(|x| -> String {x.to_string()}).collect::<Vec<String>>()[1].clone()
      };
      match delimer {
        Delimer::StartDelimer(c) => {
          get_first_split(c)
        },
        Delimer::EndDelimer(e) =>  {
          get_first_split(e)
        },
        Delimer::InlineDelimer(_) => {
          get_first_split('=')
        },
        Delimer::None => {
          sp_line
        }
      }
    };
    match block_type {
      BlockType::None => {
        line
      },
      BlockType::Inline => {
        splitter()
      },
      BlockType::Function => {
        splitter()
      },
      BlockType::Array => {
        splitter()
      }
    }
  }
}
