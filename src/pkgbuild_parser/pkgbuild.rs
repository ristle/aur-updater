use crate::pkg::dep::Dep;
use crate::pkgbuild_parser::utils::read_lines_from_file;

#[derive(Clone, Debug)]
pub struct PKGBUILD {
    pub pkgname: String,
    pub depends: Vec<Dep>,
}

impl PKGBUILD {
    pub fn new(pkg_file_path: String) -> Self {
        let pkg_map = read_lines_from_file(pkg_file_path);
        let pkg_map = pkg_map.unwrap_or_default();
        let pkg_map_predicate = pkg_map.clone();
        let get_name = |name: &str| pkg_map_predicate.clone().entry(name.to_string()).or_default().clone();

        let mut depends = pkg_map.clone()
            .entry("depends".to_string()).or_default()
            .split(" ")
            .map(|x| {
                x.split("'").collect::<Vec<&str>>()[0].to_string()
            })
            .filter(|x| {
                !x.is_empty()
            })
            .collect::<Vec<String>>();
        depends.retain(|x| {
            !x.is_empty()
        });


        let depends = depends.into_iter().map(|name| -> Dep { Dep::new(name.to_string()) }).collect::<Vec<Dep>>();

        let pkg_base_name = get_name("pkgbase");
        let pkg_name = get_name("pkgname");

        PKGBUILD {
            pkgname: if pkg_base_name.is_empty() { pkg_name.clone() } else { pkg_base_name.clone() },
            depends,
        }
    }
}