use std::fs::File;
use std::collections::HashMap;
use std::io::{BufRead, BufReader};

use crate::pkgbuild_parser::block::Block;
use crate::pkgbuild_parser::delimer::Delimer;

pub fn read_lines_from_file(file_path: String) -> std::io::Result<HashMap<String, String>> {
  let file = File::open(file_path)?;
  let reader = BufReader::new(file);

  let mut pkg_map: HashMap<String, String> = HashMap::new();
  let mut pkg_block = Block::default();

  let mut lines = reader.lines();
  while let Some(line) = lines.next() {
    let line = line.unwrap();
    let parsed_string = line.clone().split('=').map(|x| -> String {x.to_string()}).collect::<Vec<String>>();
    let current_block = Block::new(line);

    match current_block.delimer {
      Delimer::StartDelimer(s) => {
        pkg_block.name = current_block.clone().name;
        pkg_block.line = current_block.clone().line;
        pkg_block.delimer = Delimer::StartDelimer(s);
        pkg_block.block_type = current_block.block_type;
      },
      Delimer::EndDelimer(_) => {
        pkg_block.line += parsed_string[0].clone().as_str();
        pkg_map.insert(pkg_block.name, pkg_block.line);
        pkg_block = Block::default();
      },
      Delimer::InlineDelimer(_) => {
        pkg_block = current_block.clone();
        pkg_map.insert(current_block.name, current_block.line);
      }
      Delimer::None => {
        if parsed_string[0].clone().trim().chars().nth(0) != Some('#') {
          let block_without_comment = parsed_string[0].clone().trim().to_string();
          let block_without_comment = block_without_comment.split("#").collect::<Vec<_>>()[0].to_string() + " ";
          pkg_block.line += block_without_comment.as_str();
        }
      }
    }

  }

  for (_, value) in pkg_map.iter_mut() {
    *value = value.replace(&['(', ')', ',', '\"', ';', ':', '\''][..], "");
  }

  Ok(pkg_map)
}
