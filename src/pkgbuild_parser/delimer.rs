
#[derive(Default, Clone, Debug)]
pub enum Delimer {
  StartDelimer(char),
  EndDelimer(char),
  InlineDelimer(char),
  #[default]
    None
}
