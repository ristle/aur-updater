use crate::pkg::pkg_type::PkgType;

use std::process::Command;

#[derive(Clone, Debug)]
pub struct Dep {
  pub name: String,
  pub type_: PkgType,
}
static mut FOUNDED_PKGS: Vec<Dep> = vec![];

impl Dep {
  pub fn new(name: String) -> Self {
    Dep {
      name: name.clone(),
      type_: Dep::check_dep_type(name)
    }
  }

  fn find_in_pkgs(name: String) -> Option<Dep> {
    unsafe {
      FOUNDED_PKGS.clone().into_iter().find(|pkg| pkg.name == name )
    }
  }

  fn write_in_pkgs(dep: Dep) {
    unsafe {
      FOUNDED_PKGS.push(dep);
    }
  }

  pub fn check_dep_type(name: String) -> PkgType {

    match Dep::find_in_pkgs(name.clone()) {
      Some(dep) => { return dep.type_},
      None => {}
    };

    let cmd_result = Command::new("pacman")
                .arg("-Ssq")
                .arg(name.clone())
                .stdout(std::process::Stdio::piped())
                .stderr(std::process::Stdio::piped())
                .output()
                .expect("failed to compile");

    let lines = String::from_utf8(cmd_result.stdout).unwrap();

    for line in lines.lines() {
      if line == name.clone() {
        Dep::write_in_pkgs( Dep{ name: name.clone(), type_: PkgType::ARCH});
        return PkgType::ARCH
      }
    }
    Dep::write_in_pkgs( Dep{ name: name.clone(), type_: PkgType::AUR});
    PkgType::AUR
  }
}
