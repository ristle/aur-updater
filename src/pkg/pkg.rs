use std::error::Error;
use std::fmt::{Display, Formatter};
use crate::notify::{GotifyNotifications, push_notify_on_error};
use crate::pkg::dep::Dep;
use crate::pkg::pkg_type::PkgType;
use crate::pkgbuild_parser::pkgbuild::PKGBUILD;


use colored::Colorize;
use log::{error, info};
use std::process::Command;

use itertools::Itertools;
use crate::errors::{PKGCompileError, PKGDownloadError, PkgNotFound};
use crate::logger::LogUpdate;
use crate::utils::{check_aur_directory, PKGDirectory, upload_pkg_file};

#[derive(Debug, Clone, PartialEq, Eq, Hash)]
pub struct PKG {
    pub name: String,
    pub aur: PkgType,
    pub path: Option<String>,
    pub deps: Option<Vec<PKG>>,
    pub gotify: Option<GotifyNotifications>,
    pub server_upload_url: Option<String>,
}


impl Display for PKG {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        f.write_str(self.name.as_str())
    }
}

impl PKG {
    pub fn new(path: String, gotify: Option<GotifyNotifications>, server_upload_url: Option<String>) -> PKG {
        let pkg_build = PKGBUILD::new(path.clone() + "/PKGBUILD");
        PKG {
            name: pkg_build.pkgname.clone(),
            path: Some(path.clone()),
            deps: if !pkg_build.depends.is_empty() {
                PKG::process_deps(pkg_build.depends, path, gotify.clone(), server_upload_url.clone())
            } else {
                None
            },
            aur: PkgType::AUR,
            gotify,
            server_upload_url,
        }
    }

    fn get_pkg_path(path: &str) -> String {
        let binding = path.to_string();
        let mut splited_path = binding.split("/").collect::<Vec<&str>>();

        let split_path = match splited_path
            .iter()
            .any(|string: &&str| string.contains("deps"))
        {
            true => {
                splited_path.pop();
                splited_path.join("/") + "/"
            }
            false => {
                splited_path.pop();
                splited_path.join("/") + "/deps/"
            }
        };

        std::fs::create_dir_all(split_path.clone()).expect("Failed to create dir");

        split_path
    }
    fn download_dep(pkg_name: String, pkgs_path: String) -> Result<(), Box<dyn Error>> {
        let dep_pkg_path: String = pkgs_path.clone();

        match check_aur_directory(dep_pkg_path) {
            PKGDirectory::NotFound => {
                Self::download_aur_package(pkg_name, pkgs_path)?;
            }
            PKGDirectory::Empty(empty_dir) => {
                std::fs::remove_dir(empty_dir)?;
                Self::download_aur_package(pkg_name, pkgs_path)?;
            }
            PKGDirectory::Existing(_) => {}
        }
        Ok(())
    }

    fn process_dep(
        dep: Dep,
        aur_pkg_path: String,
        gotify: Option<GotifyNotifications>,
        server_upload_url: Option<String>,
    ) -> Result<Option<PKG>, Box<dyn Error>> {
        match dep.type_ {
            PkgType::ARCH => Ok(None),
            PkgType::AUR => {
                let pkg_path_parent: String = PKG::get_pkg_path(&aur_pkg_path);
                let dep_pkg_path: String = pkg_path_parent.clone() + dep.name.clone().as_str();

                Self::download_dep(dep.name, aur_pkg_path.clone())?;

                Ok(Some(PKG::new(dep_pkg_path.clone(), gotify.clone(), server_upload_url)))
            }
        }
    }
    pub fn process_deps(
        deps: Vec<Dep>,
        aur_pkg_path: String,
        gotify: Option<GotifyNotifications>,
        server_upload_url: Option<String>,
    ) -> Option<Vec<PKG>> {
        let mut pkgs: Vec<PKG> = vec![];

        for dep in deps {
            if let Ok(Some(pkg)) = PKG::process_dep(dep.clone(), aur_pkg_path.clone(), gotify.clone(), server_upload_url.clone()) {
                if !pkg.name.is_empty() {
                    pkgs.push(pkg);
                }
            }
        }

        if pkgs.is_empty() {
            None
        } else {
            Some(pkgs)
        }
    }

    pub fn download_aur_package(pkg_name: String, pkgs_path: String) -> Result<(), Box<dyn Error>> {
        match std::env::set_current_dir(pkgs_path.clone()) {
            Ok(()) => {}
            Err(err) => {
                error!("{}. Path: {}", err, pkgs_path.clone());
                return Err(PKGDownloadError.into());
            }
        }

        let pkg_url = "https://aur.archlinux.org/".to_string() + pkg_name.as_str();

        info!("Downloading -> {}", pkg_name.clone());

        let cmd_result = Command::new("git")
            .arg("clone")
            .arg(pkg_url)
            .stdout(std::process::Stdio::piped())
            .stderr(std::process::Stdio::piped())
            .spawn()?;

        let mut logger = LogUpdate::new(std::io::stdout()).expect("Failed to create Logger");

        Ok(logger.process_cmd(cmd_result).map_err(|_error| Box::new(PKGDownloadError))?)
    }

    fn check_to_rebuild(&self) -> bool {
        let cmd_result = Command::new("pacman")
            .arg("-Ql")
            .arg(&self.name)
            .stdout(std::process::Stdio::piped())
            .stderr(std::process::Stdio::piped())
            .output()
            .expect("failed to download");

        (match cmd_result.status.success() {
            true => {
                for line in String::from_utf8(cmd_result.stdout)
                    .unwrap_or_default()
                    .lines()
                {
                    let file = line.split(" ").enumerate().fold(
                        String::new(),
                        |mut string, (index, file_name)| {
                            if index > 0 && file_name.contains(".so") {
                                string += file_name;
                            }
                            string.clone()
                        },
                    );
                    let ldd_check_cmd = Command::new("ldd")
                        .arg(file)
                        .stdout(std::process::Stdio::piped())
                        .stderr(std::process::Stdio::piped())
                        .spawn()
                        .expect("failed to check ldd");

                    let not_found_ldd_cmd = Command::new("grep")
                        .arg("\"not found\"")
                        .stdin(std::process::Stdio::from(ldd_check_cmd.stdout.unwrap()))
                        .stdout(std::process::Stdio::piped())
                        .spawn()
                        .expect("Failed to find old libraries");

                    let output = not_found_ldd_cmd.wait_with_output().unwrap();
                    let result: &str = std::str::from_utf8(&output.stdout).unwrap();

                    if !result.is_empty() { return true; }
                }
                false
            }
            false => false,
        }) || !std::path::Path::new("./*zst").exists()
    }

    pub fn process(&self) -> Result<(), Box<dyn Error>> {
        if !self.check_to_rebuild() {
            info!("Skipping rebuild {}", self.name);
            return Ok(());
        }

        PKG::print_aur_start(&self.clone());

        let mut compile_pkgs: Vec<PKG> = Vec::new();

        let mut compile_pkgs_len = 1usize;

        while compile_pkgs_len != compile_pkgs.len() {
            compile_pkgs_len = compile_pkgs.len();
            self.search_empty_deps(&mut compile_pkgs);
        }

        let compile_pkgs = compile_pkgs
            .into_iter()
            .unique_by(|pkg: &PKG| pkg.name.clone())
            .collect::<Vec<PKG>>();

        if compile_pkgs.is_empty() {
            info!("No deps packages to compile!");
        }

        for pkg in compile_pkgs {
            info!("-> {} {}. Path: {}", "Compiling".green(), pkg.name, pkg.path.clone().unwrap());

            Self::download_dep(pkg.name.clone(), pkg.path.clone().unwrap())?;

            pkg.install_arch_deps().map_err(|error: Box<dyn std::error::Error>| -> String {
                error.to_string()
            })?;

            match pkg.compile() {
                Ok(()) => {
                    pkg.publish().map_err(|dyn_error: Box<dyn std::error::Error>| -> String { dyn_error.to_string() })?;
                }
                Err(err) => {
                    push_notify_on_error(self.gotify.clone(), &(err.to_string())).unwrap_or_default();
                    error!("Error: \n{}", err.to_string().red());

                    return Err(err);
                }
            };
        }

        Ok(())
    }

    pub fn publish(&self) -> Result<(), Box<dyn std::error::Error>> {
        let mut _current_dir: String = "/app/temp".to_string();

        match self.path.clone() {
            Some(path) => _current_dir = path,
            None => return Err(
                Box::new(
                    PkgNotFound {
                        name: self.name.clone(),
                        path: self.path.clone().unwrap_or_default(),
                    })),
        }

        info!("{}", self.server_upload_url.clone().unwrap_or_default());

        for entry in std::fs::read_dir(_current_dir)? {
            let entry = entry?;
            let path = entry.path();
            if path.is_file() && path.extension().unwrap_or_default() == "zst" {
                upload_pkg_file(path.to_str().unwrap_or_default(),
                                self.name.clone(),
                                &(self.server_upload_url.clone().unwrap_or_default()))?;
            }
        }

        Ok(())
    }

    pub fn install_arch_deps(&self) -> Result<(), Box<dyn std::error::Error>> {
        let deps = self.deps.clone().unwrap_or_default().iter().filter_map(
            |pkg: &PKG| -> Option<PKG> {
                match pkg.aur {
                    PkgType::ARCH => Some(pkg.clone()),
                    PkgType::AUR => None
                }
            }
        ).collect::<Vec<PKG>>().iter().join(" ").clone();

        if deps.trim().is_empty() { return Ok(()); }

        let cmd_result = Command::new("pacman")
            .arg("-Sy")
            .arg(deps)
            .stdout(std::process::Stdio::piped())
            .stderr(std::process::Stdio::piped())
            .spawn()?;

        let mut logger = LogUpdate::new(std::io::stdout()).expect("Failed to create Logger");

        logger.process_cmd(cmd_result)
    }

    pub fn compile(&self) -> Result<(), Box<dyn Error>> {
        match std::env::set_current_dir(self.path.clone().unwrap()) {
            Ok(()) => {}
            Err(_) => {
                return Err(PKGCompileError { msg: "Can not set dir".to_string() }.into());
            }
        }

        if !std::path::Path::new(&(self.path.clone().unwrap() + "/PKGBUILD")).exists() {
            return Ok(());
        }


        let cmd_result = Command::new("makepkg")
            .arg("-Csi")
            .arg("--noconfirm")
            .stdout(std::process::Stdio::piped())
            .stderr(std::process::Stdio::piped())
            .spawn()?;

        let mut logger = LogUpdate::new(std::io::stdout()).expect("Failed to create Logger");

        logger.process_cmd(cmd_result)
    }

    pub fn search_empty_deps(&self, compile_pkgs: &mut Vec<PKG>) {
        match self.deps.clone() {
            Some(deps) => {
                let mut super_deps = deps.clone();
                super_deps.retain(|x| !compile_pkgs.contains(x));

                if super_deps.is_empty() {
                    if !compile_pkgs.contains(&self.clone()) {
                        compile_pkgs.push(self.clone());
                    }
                } else {
                    for a in deps {
                        a.search_empty_deps(compile_pkgs);
                    }
                }
            }
            None => {
                if !compile_pkgs.contains(&self) {
                    compile_pkgs.push(self.clone());
                }
            }
        }
    }

    fn print_aur(prefix: String, pkg: Option<PKG>, last: bool) {
        match pkg {
            Some(pkg) => {
                println!(
                    "{}{}{}",
                    prefix,
                    if last { "└──" } else { "├──" },
                    pkg.name.purple()
                );
                let deps = pkg.deps.unwrap_or_default();
                for (index, dep) in deps.iter().enumerate() {
                    PKG::print_aur(
                        prefix.clone() + (if last { "    " } else { "│   " }),
                        Some(dep.clone()),
                        index == (deps.len() - 1),
                    );
                }
            }
            None => {}
        }
    }

    pub fn print_aur_start(&self) {
        for dep in self.deps.clone().unwrap_or_default() {
            print!("{} ", dep.name);
        }
        println!();

        PKG::print_aur("".to_string(), Some(self.clone()), false);
    }
}
